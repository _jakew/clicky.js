// Get jQuery if necessary
window.onload = function() {
  if (window.jQuery) {
    // jQuery is here already
    console.log('Clicky: jQuery is present');
  }
  else {
    console.log('Clicky: loading jQuery');
    // load jQuery
    var jQuery = document.createElement('script');
    jQuery.src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js";
    document.head.appendChild(jQuery);
  }
}

// Get firebase
$.getScript('https://cdn.firebase.com/js/client/2.4.2/firebase.js', function () {
  // Setup database
  var database = new Firebase("https://clicky.firebaseio.com/");
  var clicks = 0;
  // Auto refresh click amount
  database.child("clicks").on("value", function(snapshot) {
    // Find button and change text
    $("#clickyButton").html("Clicked " + snapshot.val() + " times");
    // Set variable
    clicks = parseInt(snapshot.val());
  });
  // Increment click amount on button click
  $("#clickyButton").bind('click', function() {
    // Get new amount
    newClicks = clicks + 1;
    // Send new amount to database
    database.child("clicks").set(newClicks);
    // Prevent the link from going anywhere
    return false;
  });
});