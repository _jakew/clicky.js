# Clicky.js
Clicky.js is a small project made by me. It **does** rely on Firebase as the backend. It is basically a button that you can put onto your website somewhere. When people click it, it will update the button where ever else it is embedded.

## Features
* The button embedded is easy to customize (see below).
* The button is global and updates on everyone's websites when clicked.

## Setup
First you'll need to include the clicky.js library:
`<script src="location/of/library/clicky.js"></script>`
then you'll need to create a button or link (or any other element):
`<a href="" id="clickyButton"></a>` or `<button type="button" id="clickyButton"></button>`

## Customization
As you create the element yourself in the HTML code rather than the Javascript file, clicky.js is very customizeable. For all of these I'll be using a link.

### Loading text
e.g `<a href="" id="clickyButton">Loading...</a>`, `<a href="" id="clickyButton"><i>Please Wait</i></a>`

### Bootstrap Button
`<a href="" id="clickyButton" class="btn btn-primary">Loading...</a>`

You can change anything you want about the element apart from the inside HTML after it's loaded.